var mongoose = require('mongoose'),
    Blog = mongoose.model('blog'),
    path         = require('path');

exports.show = function(req,res){
	Blog.find({}, function(err, data){
    	if(err){
    		console.log(err);
      		res.send(404);
    	}
    	res.send(data);
  	});
}

exports.create = function(req,res){
	//render the blog creation page	
}

exports.doCreate = function(req,res){
	var data = new Blog(req.body);
    data.save(function(err){
    	if(!err){
    		res.send(200);
    	}
    });
} 

exports.displayBlog = function(req,res){
	Blog.findOne({_id:req.params.id},function(err,data){
		if(err){
			console.log(err);
			res.send(404);
		}
		res.send(data);
	})
}

exports.edit = function(req,res){
	//render blog edit page
	Blog.findOne({_id:req.params.id},function(err,data){
		if(err){
			console.log(err);
			res.send(404);
		}
		res.send(data);
	})
}

exports.doEdit = function(req,res){
	Blog.update(req.body,{upsert:true},function(err){
		if(!err){
			console.log("success");
		}
	})
}

exports.confirmDelete = function(req,res){

}

exports.doDelete = function(req,res){
	Blog.remove({_id:req.params.id},function(err){
		if(!err){
			console.log("removed");
		}
	})
}
var mongoose = require('mongoose'),
    kabaddiScore = mongoose.model('kabaddiScore'),
    path         = require('path');


var justOnce = true;
io.on('connection', function(socket){
    if(justOnce){
        justOnce = false;
        kabaddiScore.subscribe(function(data){
            io.emit('newLiveScore', data); 
        });
    }
});

exports.doSave = function(req,res){
    delete req.body._id;
    var data = new kabaddiScore(req.body);
    data.save(function(err){
    });
    res.sendStatus(200);
}

exports.show = function(req,res){
    res.sendFile(path.join(__dirname,'../public','admin.html'));
    kabaddiScore.findOne().sort('-updatedOn').exec(function(err,score){
        if(!err){
            console.log(score);
        }
    })
}
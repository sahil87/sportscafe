var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;

var blogSchema = new Schema({
    title      : String,
    writer     : String,
    blog       : String,
    img_url    : String,
    createdOn  : Date,
    modifiedOn : {type: Date, default: Date.now }

});

module.exports = mongoose.model('blog', blogSchema);
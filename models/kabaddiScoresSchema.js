var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;

var kabaddiScoreSchema = new Schema({
    teamname_1 : String,
    teamname_2 : String,
    points_1   : String,
    points_2   : String,
    updatedOn  : {type: Date, default: Date.now }
});

var subscribers = [];

kabaddiScoreSchema.post('save', function(){
    console.log(subscribers, 'saving');
    for(var i in subscribers){
        subscribers[i](this);
    }
    return true;
});

kabaddiScoreSchema.static('subscribe', function(subscriber){
    subscribers.push(subscriber);
});

module.exports = mongoose.model('kabaddiScore', kabaddiScoreSchema);

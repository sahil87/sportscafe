var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;

var teamSchema = new Schema({
    id         : Number,
    name       : String,
    manager    : {id: Number, name: String},
    coach      : {id: Number, name: String},
    player     : {
  					id        : Number,
  					name      : String,
  					country   : String,
  					franchise : String,
  					gender    : String,
  					tob       : Date, // time of birth
  					pob       : String, // place of birth
  					cob       : String, // country of birth
  					height    : Number,
  					weight    : Number,
  					photo_url : String,
  					type      : String,
  					totalRaids: Number,
  					succRaids : Number,
  					bonusPoints : Number,
  					tackesmade: Number,
  					succTackles: Number 
				},
    createdOn  : Date,
    modifiedOn : {type: Date, default: Date.now }

});

module.exports = mongoose.model('team', teamSchema);
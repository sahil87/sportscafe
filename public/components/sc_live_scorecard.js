// how about adding auto-update as well?
var socket = require('socket.io-client')('http://localhost:8000');

scores = {teamname_1:"India",teamname_2:"Australia",points_1:10,points_2:15};

function render(){
	var sc_live_scorecard = $('#sc_live_scorecard').html();
	var template = handlebars.compile(sc_live_scorecard);
	var html=template(scores);
	var a=$.parseHTML(html)
	$('#sc_live_scorecard_a').html(a);
}

render();
/*key bindings*/
$('#sc_live_scorecard_a').on("click", '.team1_plus',function(){
	scores.points_1+=1;
	render();
});
$('#sc_live_scorecard_a').on("click", ".team1_minus",function(){
	scores.points_1 -=1;
	render();
});

$('#sc_live_scorecard_a').on("click",".team2_plus", function(){
	scores.points_2 +=1;
	render();
});

$('#sc_live_scorecard_a').on("click",".team2_minus", function(){
	scores.points_2 -=1;
	render();
});

$('#sc_live_scorecard_a').on("click",'.submit',function(event){
	event.preventDefault();
	console.log(scores);
	$.ajax({type:"POST",
			url: "/api/update",
			data: scores,
			success: function(data){
				console.log(data);
			},
			error:function(){
				console.log("sdf");
			}
		});
});

socket.on('newLiveScore', function(data){
    scores= data;
    scores.points_1 = parseInt(scores.points_1);
    scores.points_2 = parseInt(scores.points_2);
    render();
});


var json = {
  links: [
    { url: "/register", title: "Register" },
    { url: "/login", title: "Login" }
  ]
}
var sc_navbar = $('#sc_navbar').html();

handlebars.registerHelper('each', function(context, options) {
  var ret = "";

  for(var i=0, j=context.length; i<j; i++) {
    ret = ret + options.fn(context[i]);
  }

  return ret;
});

var template = handlebars.compile(sc_navbar);
var html=template(json);
var a=$.parseHTML(html)
$('#sc_navbar').html(a);
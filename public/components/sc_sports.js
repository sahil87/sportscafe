
var json = {
  sports: [
    { url: "/kabaddi", title: "Kabaddi" },
    { url: "/cricket", title: "Cricket" },
    { url: "/football", title: "Football" },
    { url: "/badminton", title: "Badminton" }
  ]
}
var sc_sports = $('#sc_sports').html();

handlebars.registerHelper('each', function(context, options) {
  var ret = "";

  for(var i=0, j=context.length; i<j; i++) {
    ret = ret + options.fn(context[i]);
  }

  return ret;
});

var template = handlebars.compile(sc_sports);
var html=template(json);
var a=$.parseHTML(html)
$('#sc_sports').html(a);
var path  = require('path');

var kabaddiScore = require('./controllers/kabaddiScore');
var blog         = require('./controllers/blog');

module.exports = function(app){
	app.get('/',function(req,res){

	})

	app.get('/admin',kabaddiScore.show);

	app.get('/admin/blog',blog.show);
	app.get('/admin/blog/new',blog.create);
	app.post('/admin/blog/new',blog.doCreate);
	app.get('/admin/blog/:id',blog.displayBlog);
	app.get('/admin/blog/edit/:id',blog.edit);
	app.post('/admin/blog/edit/:id',blog.doEdit);
	app.get('/admin/blog/delete/:id',blog.confirmDelete);
	app.post('/admin/blog/delete/:id',blog.doDelete);

	app.post('/api/update',kabaddiScore.doSave);
};
/*
  Common schema definition used across all sports
  Almost all sports have a concept of tournaments and matches
  Anything that doesn't fall in a tournament can be assumed
  to fall in a tournament named generic
*/
/*
Tournaments contain matches contain scores
var tournametSchema = new Schema({
  tournamentName: String,
  sport: String,
  startDate: Date,
  endDate: Date,
  matches: [{
    id: Number,
    teams: [{
      id: Number,
      name: String,
      manager: {id: Number, name: String},
      coach: {id: Number, name: String},
      players: [{
        id: Number,
        name: String,
        jerseyNo: Number,
        isCaptain: Boolean,
      }],
    }],
    rounds: [{
      id: Number, //Rest changes according to sport
  }]
  }],
});
*/

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var _ = require('lodash');

var playerObject = {
  id: Number,
  name: String,
  jerseyNo: Number,
  isCaptain: Boolean,
};

var teamObject = function (playerObjectPassed) {
  return {
    id: Number,
    name: String,
    manager: {id: Number, name: String},
    coach: {id: Number, name: String},
    players: [playerObjectPassed]
  };
}();

var matchObject = function (teamObjectPassed) {
  return {
    id: Number,
    dateTime: Date,
    venue: {id: Number, name: String},
    teams: [teamObjectPassed]
  };
}();

var tournamentObject = function (matchObjectPassed) {
  return {
    tournamentName: String,
    sport: String,
    startDate: Date,
    endDate: Date,
    matches: [matchObjectPassed],
  };
}();

var kabaddiMatchExtender = {
  raids: [{
    raiderTeamId: Number,
    antiTeamId: Number,
    raidDuration: Number,
    raidStartTime: Date,
    raidEndTime: Date,
    raidFrom: String, //left or right
    raider: {
      playerId: Number,
      name: String
    },
    anti: {
      playerId: Number,
      name: String
    }
  }]
};

var kabaddiMatchObject = _.extend(_.extend({}, matchObject), kabaddiMatchExtender);

var kabaddiTournamentObject = tournamentObject(kabaddiMatchObject);

var kabaddiTournametSchema = new Schema(kabaddiTournamentObject);

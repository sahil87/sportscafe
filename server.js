var express  = require('express');
var logger   = require('morgan');
var mongoose = require('mongoose');
var Schema   = mongoose.Schema;
var path     = require('path');
var app      = express();
var server   = require('http').Server(app);
var port     = process.env.PORT || 8000;
var fs       = require('fs');
io       = require('socket.io').listen(server);
var bodyParser = require('body-parser')


app.use(logger('dev'));
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

var modelsPath = path.join(__dirname, './models');
fs.readdirSync(modelsPath).forEach(function (file) {
  if (/(.*)\.(js$|coffee$)/.test(file)) {
    require(modelsPath + '/' + file);
  }
});
require('./routes')(app);

server.listen(port);
mongoose.connect('mongodb://localhost/sportscafe');
console.log("Server Listening On Port " + port);
